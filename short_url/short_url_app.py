from flask import Flask

from short_url.apis import api


class ShortUrlApp(Flask):
    def __init__(self, import_name):
        super().__init__(import_name)
        self.db_session = None
        self.db_engine = None


def create_app() -> ShortUrlApp:
    app = ShortUrlApp("short_url")
    api.init_app(app)
    return app
