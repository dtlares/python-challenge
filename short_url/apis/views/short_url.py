from flask import Response
from flask_restplus import Namespace, Resource
from werkzeug.exceptions import BadRequest, Conflict, NotFound

from short_url.apis.marshaling import (ShortenIn, ShortenOut, SortcodeStatsOut,
                                       marshal)
from short_url.apis.services.short_url import (ShortcodeInUseException,
                                               create_short_code,
                                               get_statistics, get_url)

shorten_api = Namespace("", description="Shorten url endpoints")


shorten_url_request = shorten_api.schema_model("Shorten (Request)", ShortenIn.schema())

shorten_url_response = shorten_api.schema_model(
    "Shorten (Response)", ShortenOut.schema()
)


shorten_stats_response = shorten_api.schema_model(
    "Shorcode Stats (Response)", SortcodeStatsOut.schema()
)


@shorten_api.route("/shorten")
class ShortenApi(Resource):
    @shorten_api.expect(shorten_url_request)
    @shorten_api.response(201, "Created", shorten_url_response)
    @shorten_api.response(400, "Not present")
    @shorten_api.response(409, "Shortcode in use")
    @shorten_api.response(422, "Bad formed shorcode")
    def post(self):
        payload = ShortenIn(**self.api.payload)

        if not payload.url:
            raise BadRequest("Missing url in body payload.")
        try:
            shortcode = create_short_code(payload)
        except ShortcodeInUseException as e:
            raise Conflict(str(e))
        return marshal(shortcode), 201


@shorten_api.route("/<short_code>")
class ShortCodeApi(Resource):
    @shorten_api.response(302, "Found")
    @shorten_api.response(404, "Not Found")
    def get(self, short_code: str):
        url = get_url(short_code)
        if not url:
            raise NotFound(f"No url is set for {short_code} code.")
        response = Response(status=302)
        response.headers["Location"] = url
        return response


@shorten_api.route("/<short_code>/stats")
class ShortCodeStatsApi(Resource):
    @shorten_api.response(200, "Found", shorten_stats_response)
    @shorten_api.response(404, "Not Found")
    def get(self, short_code):
        statistics = get_statistics(short_code)
        if not statistics:
            raise NotFound(f"No url is set for {short_code} code.")
        return marshal(statistics)
