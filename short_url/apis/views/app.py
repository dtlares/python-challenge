from flask_restplus import Namespace, Resource

app_api = Namespace("app", description="App healtcheck endpoints")


@app_api.route("/ping")
class HealthcheckApi(Resource):
    def get(self):
        return "pong"
