from typing import Optional

from short_url.apis.models import Shortcode

_SHORT_CODE_DB = {}


class ShortcodeRepository:
    def get_shortcode(self, shortcode: Optional[str]) -> Optional[Shortcode]:
        if not shortcode:
            return None
        return _SHORT_CODE_DB.get(shortcode, None)

    def set_shortcode(self, shortcode: Shortcode):
        _SHORT_CODE_DB[shortcode.shortcode] = shortcode
