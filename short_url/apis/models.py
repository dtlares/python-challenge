from dataclasses import dataclass, field
from datetime import datetime
from typing import Optional


@dataclass
class Shortcode:
    shortcode: str
    url: str
    start_date: datetime = field(default_factory=datetime.now)
    redirect_count: int = 0
    last_seen_date: Optional[datetime] = None
