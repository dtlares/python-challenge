from datetime import datetime
from typing import Optional

import exrex

from short_url.apis.marshaling import ShortenIn, ShortenOut, SortcodeStatsOut
from short_url.apis.models import Shortcode
from short_url.apis.repositories.short_code import ShortcodeRepository

_SHORT_CODE_REGEX = "^[0-9a-zA-Z_]{6}$"


class ShortcodeInUseException(Exception):
    pass


def _is_short_code_in_use(code) -> bool:
    return ShortcodeRepository().get_shortcode(code) is not None


def generate_random_code() -> str:
    while True:
        generated_code = exrex.getone(_SHORT_CODE_REGEX)
        if not _is_short_code_in_use(generated_code):
            break
    return generated_code


def create_short_code(payload: ShortenIn) -> ShortenOut:
    shortcode_repository = ShortcodeRepository()
    shortcode_in_db = shortcode_repository.get_shortcode(payload.shortcode)
    if shortcode_in_db:
        raise ShortcodeInUseException(f"Code {payload.shortcode} already in use.")

    new_code = payload.shortcode if payload.shortcode else generate_random_code()
    shortcode = Shortcode(shortcode=new_code, url=payload.url)
    shortcode_repository.set_shortcode(shortcode)
    return ShortenOut.construct(shortcode=shortcode.shortcode)


def get_url(sortcode: str) -> Optional[str]:
    shortcode_repository = ShortcodeRepository()
    shortcode_in_db = shortcode_repository.get_shortcode(sortcode)

    if not shortcode_in_db:
        return None

    shortcode_in_db.redirect_count += 1
    shortcode_in_db.last_seen_date = datetime.now()
    shortcode_repository.set_shortcode(shortcode_in_db)

    return shortcode_in_db.url


def get_statistics(sortcode: str) -> Optional[SortcodeStatsOut]:
    shortcode_repository = ShortcodeRepository()
    shortcode_in_db = shortcode_repository.get_shortcode(sortcode)

    if not shortcode_in_db:
        return None

    return SortcodeStatsOut.construct(
        start_date=shortcode_in_db.start_date,
        last_seen_date=shortcode_in_db.last_seen_date,
        redirect_count=shortcode_in_db.redirect_count,
    )
