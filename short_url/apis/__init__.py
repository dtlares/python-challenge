from flask_restplus import Api

from short_url.apis.views.app import app_api
from short_url.apis.views.short_url import shorten_api

api = Api(
    title="Short Url",
    version="0.0.1",
    description="Application used to shorten urls",
    doc="/swagger/",
)


api.add_namespace(app_api)
api.add_namespace(shorten_api)
