import json
import re
from datetime import datetime
from typing import Any, Dict, Optional

from pydantic import BaseModel, validator  # pylint: disable=no-name-in-module
from werkzeug.exceptions import UnprocessableEntity

_SHORT_CODE_MATCH_REGEX = "^[0-9a-zA-Z_]{4,}$"


class InputModel(BaseModel):
    pass


class OutputModel(BaseModel):
    class Config:
        orm_mode = True
        allow_population_by_field_name = True
        json_encoders = {
            datetime: lambda v: v.isoformat(timespec="milliseconds") + "Z",
        }


class ShortenIn(InputModel):

    url: Optional[str] = None
    shortcode: Optional[str]

    @validator("shortcode")
    def check_shortcode(cls, v):
        if v and not bool(re.match(_SHORT_CODE_MATCH_REGEX, v)):
            raise UnprocessableEntity("Invalid shortcode input format.")
        return v


class ShortenOut(OutputModel):
    shortcode: str


class SortcodeStatsOut(OutputModel):
    start_date: datetime
    last_seen_date: datetime
    redirect_count: int


def marshal(output_model: OutputModel) -> Dict[str, Any]:
    return json.loads(output_model.json(indent=1))
