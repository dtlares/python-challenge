from behave import *

from features.steps.common import BASE_URL


@step("a request to get a short url with code '{a_code}'")
def step_impl(context, a_code: str):
    context.url = BASE_URL + f"/{a_code}"


@when("the Short Url App receives a get to an url from a short code")
def step_impl(context):
    context.response = context.client.get(context.url)
