from behave import *

from features.steps.builders import ShortCodeBuilder
from short_url.apis.repositories.short_code import _SHORT_CODE_DB

BASE_URL = "http://127.0.0.1"


@given("the Short Url App working")
def step_impl(context):
    pass


@step("the app notifies that the request was successfull")
def step_impl(context):
    assert context.response.status_code == 200


@step("the app notifies that the creation was successfull")
def step_impl(context):
    assert context.response.status_code == 201


@step("the app notifies that there is a bad request")
def step_impl(context):
    assert context.response.status_code == 400


@step("the app notifies that there is a conflict")
def step_impl(context):
    assert context.response.status_code == 409


@step("the app notifies that there is an unprocessable entity")
def step_impl(context):
    assert context.response.status_code == 422


@step("the app response is not found")
def step_impl(context):
    assert context.response.status_code == 404


@step("the application has an existing '{a_code}' short code with url '{an_url}'")
def step_impl(context, a_code: str, an_url: str):
    context.existing_shortcode = (
        ShortCodeBuilder().with_shortcode(a_code).with_url(an_url).get_model()
    )
    _SHORT_CODE_DB[a_code] = context.existing_shortcode


@step(
    "the application has an existing '{a_code}' short code with visit count equals to '{count}'"
)
def step_impl(context, a_code: str, count: str):
    context.existing_shortcode = (
        ShortCodeBuilder()
        .with_shortcode(a_code)
        .with_redirect_count(int(count))
        .get_model()
    )
    _SHORT_CODE_DB[a_code] = context.existing_shortcode


@step("the redirect count is equal to '{count}'")
def step_impl(context, count: str):
    assert context.response.json["redirect_count"] == int(count)


@step("the app redirects successfull to '{an_url}'")
def step_impl(context, an_url: str):
    assert context.response.status_code == 302
    assert context.response.headers["Location"] == "http://127.0.0.1/an_url"
