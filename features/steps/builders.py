from short_url.apis.models import Shortcode


class ShortCodeBuilder:
    def __init__(self):
        self._shortcode = Shortcode(shortcode="foo", url="bar")

    def with_shortcode(self, shortcode: str) -> "ShortCodeBuilder":
        self._shortcode.shortcode = shortcode
        return self

    def with_url(self, url: str) -> "ShortCodeBuilder":
        self._shortcode.url = url
        return self

    def with_redirect_count(self, count: int) -> "ShortCodeBuilder":
        self._shortcode.redirect_count = count
        return self

    def get_model(self) -> Shortcode:
        return self._shortcode
