from behave import *

from features.steps.common import BASE_URL


@step("a request to create a short url with code '{a_code}' and url '{an_url}'")
def step_impl(context, a_code: str, an_url: str):
    context.payload = dict(url=an_url, shortcode=a_code)
    context.url = BASE_URL + "/shorten"


@step("a request to create a short url with code '{a_code}' and no url")
def step_impl(context, a_code: str):
    context.payload = dict(shortcode=a_code)
    context.url = BASE_URL + "/shorten"


@when("the Short Url App receives a request to create a code")
def step_impl(context):
    context.response = context.client.post(context.url, json=context.payload)


@step("the response includes '{a_code}' short code")
def step_impl(context, a_code: str):
    data = context.response.json
    assert data["shortcode"] == a_code
