from behave import fixture, use_fixture

from short_url.apis.repositories.short_code import _SHORT_CODE_DB
from short_url.short_url_app import create_app


@fixture
def client(context, *args, **kwargs):
    app = create_app()
    context.current_app = app
    context.client = app.test_client()
    yield context.client


def before_scenario(context, scenario):
    use_fixture(client, context)
    global _SHORT_CODE_DB
    _SHORT_CODE_DB = {}
