Feature: Get Short Url

Scenario: Happy Path
  Given the Short Url App working
    And a request to get a short url with code 'a_code'
    And the application has an existing 'a_code' short code with url 'an_url'
  When the Short Url App receives a get to an url from a short code
  Then the app redirects successfull to 'an_url'


Scenario: Code is not present
  Given the Short Url App working
    And a request to get a short url with code 'missing_code'
  When the Short Url App receives a get to an url from a short code
  Then the app response is not found
