Feature: Create Short Url

Scenario: Happy Path
  Given the Short Url App working
    And a request to create a short url with code 'a_code' and url 'an_url'
  When the Short Url App receives a request to create a code
  Then the app notifies that the creation was successfull
  And the response includes 'a_code' short code


Scenario: Invalid code
  Given the Short Url App working
    And a request to create a short url with code '$invalid$' and url 'an_url'
  When the Short Url App receives a request to create a code
  Then the app notifies that there is an unprocessable entity


Scenario: Missing Url
  Given the Short Url App working
    And a request to create a short url with code 'a_code' and no url
  When the Short Url App receives a request to create a code
  Then the app notifies that there is a bad request


Scenario: Url in use
  Given the Short Url App working
    And a request to create a short url with code 'a_code' and url 'an_url'
    And the application has an existing 'a_code' short code with url 'an_url'
  When the Short Url App receives a request to create a code
  Then the app notifies that there is a conflict
