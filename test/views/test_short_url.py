import pytest
from werkzeug.exceptions import BadRequest, Conflict, NotFound

from short_url.apis.services.short_url import ShortcodeInUseException
from short_url.apis.views.short_url import (ShortCodeApi, ShortCodeStatsApi,
                                            ShortenApi)


class TestShortenApi:
    def test_post(self, mocker):
        payload_model_mock = mocker.Mock()
        shortcode_model_mock = mocker.Mock()
        api = mocker.MagicMock(payload=mocker.MagicMock(spec=dict))
        shorten_model_mock = mocker.patch(
            "short_url.apis.views.short_url.ShortenIn", return_value=payload_model_mock
        )
        create_short_code_mock = mocker.patch(
            "short_url.apis.views.short_url.create_short_code",
            return_value=shortcode_model_mock,
        )
        marshal_mock = mocker.patch("short_url.apis.views.short_url.marshal")

        result = ShortenApi(api=api).post()

        shorten_model_mock.assert_called_once_with(**api.payload)
        create_short_code_mock.assert_called_once_with(payload_model_mock)
        marshal_mock.assert_called_once_with(shortcode_model_mock)
        assert result == (marshal_mock.return_value, 201)

    def test_post_missing_url(self, mocker):
        payload_model_mock = mocker.MagicMock()
        payload_model_mock.url = None
        api = mocker.MagicMock(payload=mocker.MagicMock(spec=dict))
        shorten_model_mock = mocker.patch(
            "short_url.apis.views.short_url.ShortenIn", return_value=payload_model_mock
        )

        with pytest.raises(BadRequest):
            ShortenApi(api=api).post()

        shorten_model_mock.assert_called_once_with(**api.payload)

    def test_post_short_code_in_use(self, mocker):
        payload_model_mock = mocker.Mock()
        api = mocker.MagicMock(payload=mocker.MagicMock(spec=dict))
        shorten_model_mock = mocker.patch(
            "short_url.apis.views.short_url.ShortenIn", return_value=payload_model_mock
        )
        create_short_code_mock = mocker.patch(
            "short_url.apis.views.short_url.create_short_code",
            side_effect=ShortcodeInUseException(),
        )

        with pytest.raises(Conflict):
            ShortenApi(api=api).post()

        shorten_model_mock.assert_called_once_with(**api.payload)
        create_short_code_mock.assert_called_once_with(payload_model_mock)


class TestShortCodeApi:
    def test_get(self, mocker):
        get_url_mock = mocker.patch(
            "short_url.apis.views.short_url.get_url", return_value="bar"
        )
        response_object_mock = mocker.MagicMock()
        response_object_mock.headers = {}
        response_mock = mocker.patch(
            "short_url.apis.views.short_url.Response", return_value=response_object_mock
        )

        result = ShortCodeApi().get(short_code="foo")

        get_url_mock.assert_called_once_with("foo")
        response_mock.assert_called_once_with(status=302)

        assert result == response_mock.return_value
        assert response_object_mock.headers["Location"] == "bar"

    def test_get_code_not_found(self, mocker):
        get_url_mock = mocker.patch(
            "short_url.apis.views.short_url.get_url", return_value=None
        )

        with pytest.raises(NotFound):
            ShortCodeApi().get(short_code="foo")

        get_url_mock.assert_called_once_with("foo")


class TestShortCodeStatsApi:
    def test_get(self, mocker):
        get_statistics_mock = mocker.patch(
            "short_url.apis.views.short_url.get_statistics"
        )
        marshal_mock = mocker.patch("short_url.apis.views.short_url.marshal")

        result = ShortCodeStatsApi().get(short_code="foo")

        get_statistics_mock.assert_called_once_with("foo")
        marshal_mock.assert_called_once_with(get_statistics_mock.return_value)
        assert result == marshal_mock.return_value

    def test_get_code_not_found(self, mocker):
        get_statistics_mock = mocker.patch(
            "short_url.apis.views.short_url.get_statistics", return_value=None
        )

        with pytest.raises(NotFound):
            ShortCodeStatsApi().get(short_code="foo")

        get_statistics_mock.assert_called_once_with("foo")
