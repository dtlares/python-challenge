import pytest

from short_url.apis.marshaling import ShortenIn
from short_url.apis.repositories.short_code import ShortcodeRepository
from short_url.apis.services.short_url import (ShortcodeInUseException,
                                               create_short_code)


def test_create_short_code(mocker):
    get_shortcode_mocker = mocker.patch.object(
        ShortcodeRepository, "get_shortcode", return_value=None
    )
    set_shortcode_mocker = mocker.patch.object(ShortcodeRepository, "set_shortcode")
    shortcode_mocker = mocker.patch("short_url.apis.services.short_url.Shortcode")
    shorten_out_mocker = mocker.patch(
        "short_url.apis.services.short_url.ShortenOut.construct"
    )
    payload_mock = mocker.MagicMock(spec=ShortenIn)
    payload_mock.shortcode = mocker.MagicMock()
    payload_mock.url = mocker.MagicMock()

    result = create_short_code(payload_mock)

    get_shortcode_mocker.assert_called_once_with(payload_mock.shortcode)
    shortcode_mocker.assert_called_once_with(
        shortcode=payload_mock.shortcode, url=payload_mock.url
    )
    set_shortcode_mocker.assert_called_once_with(shortcode_mocker.return_value)
    shorten_out_mocker.assert_called_once_with(
        shortcode=shortcode_mocker.return_value.shortcode
    )
    assert result == shorten_out_mocker.return_value


def test_create_short_code_in_use(mocker):
    get_shortcode_mocker = mocker.patch.object(ShortcodeRepository, "get_shortcode")
    payload_mock = mocker.MagicMock(spec=ShortenIn)
    payload_mock.shortcode = mocker.MagicMock()

    with pytest.raises(ShortcodeInUseException):
        create_short_code(payload_mock)

    get_shortcode_mocker.assert_called_once_with(payload_mock.shortcode)


def test_create_short_code_no_code(mocker):
    get_shortcode_mocker = mocker.patch.object(
        ShortcodeRepository, "get_shortcode", return_value=None
    )
    set_shortcode_mocker = mocker.patch.object(ShortcodeRepository, "set_shortcode")
    shortcode_mocker = mocker.patch("short_url.apis.services.short_url.Shortcode")
    shorten_out_mocker = mocker.patch(
        "short_url.apis.services.short_url.ShortenOut.construct"
    )
    generate_random_code_mocker = mocker.patch(
        "short_url.apis.services.short_url.generate_random_code"
    )
    payload_mock = mocker.MagicMock(spec=ShortenIn)
    payload_mock.shortcode = None
    payload_mock.url = mocker.MagicMock()

    result = create_short_code(payload_mock)

    get_shortcode_mocker.assert_called_once_with(payload_mock.shortcode)
    shortcode_mocker.assert_called_once_with(
        shortcode=generate_random_code_mocker.return_value, url=payload_mock.url
    )
    set_shortcode_mocker.assert_called_once_with(shortcode_mocker.return_value)
    shorten_out_mocker.assert_called_once_with(
        shortcode=shortcode_mocker.return_value.shortcode
    )
    assert result == shorten_out_mocker.return_value
