from short_url.apis.models import Shortcode


class TestShortCode:
    def test_class(self, mocker):
        start_date = mocker.MagicMock()
        last_seen_date = mocker.MagicMock()
        data = dict(
            shortcode="foo",
            url="bar",
            start_date=start_date,
            last_seen_date=last_seen_date,
        )

        result = Shortcode(**data)

        assert result.shortcode == "foo"
        assert result.url == "bar"
        assert result.start_date is start_date
        assert result.last_seen_date is last_seen_date
