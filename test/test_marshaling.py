from datetime import datetime

import pytest
from werkzeug.exceptions import UnprocessableEntity

from short_url.apis.marshaling import (OutputModel, ShortenIn, ShortenOut,
                                       SortcodeStatsOut, marshal)


class TestShortenIn:
    def test_init(self, mocker):
        data = dict(
            shortcode="foobaz",
            url="bar",
        )

        result = ShortenIn(**data)

        assert result.shortcode == "foobaz"
        assert result.url == "bar"

    def test_init_defaults(self, mocker):
        data = dict(
            shortcode="foobaz",
        )

        result = ShortenIn(**data)

        assert result.shortcode == "foobaz"
        assert result.url is None

    def test_init_invalid_code(self, mocker):
        data = dict(
            shortcode="$foobaz$",
            url="bar",
        )

        with pytest.raises(UnprocessableEntity):
            ShortenIn(**data)


class TestShortenOut:
    def test_init(self, mocker):
        shortcode_mock = mocker.MagicMock()

        result = ShortenOut.construct(shortcode=shortcode_mock)
        assert result.shortcode is shortcode_mock


class TestSortcodeStatsOut:
    def test_init(self, mocker):
        start_date = datetime.now()
        last_seen_date = datetime.now()
        redirect_count = 42

        result = SortcodeStatsOut.construct(
            start_date=start_date,
            redirect_count=redirect_count,
            last_seen_date=last_seen_date,
        )

        assert result.redirect_count == 42
        assert result.start_date is start_date
        assert result.last_seen_date is last_seen_date


def test_marshal(mocker):
    json_loads_mocker = mocker.patch("short_url.apis.marshaling.json.loads")
    output_model_mock = mocker.Mock(spec=OutputModel)

    result = marshal(output_model_mock)

    assert result == json_loads_mocker.return_value
    output_model_mock.json.assert_called_once_with(indent=1)
    json_loads_mocker.assert_called_once_with(output_model_mock.json.return_value)
