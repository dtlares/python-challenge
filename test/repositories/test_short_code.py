from short_url.apis.repositories.short_code import (_SHORT_CODE_DB,
                                                    ShortcodeRepository)


class TestShorcodeRepository:
    def test_init(self):
        assert ShortcodeRepository()

    def test_get_short_code(self, mocker):
        shortcode_mock = mocker.MagicMock()
        _SHORT_CODE_DB["foo"] = shortcode_mock

        result = ShortcodeRepository().get_shortcode("foo")
        assert result is shortcode_mock

    def test_get_short_code_missing(self, mocker):
        assert ShortcodeRepository().get_shortcode("bar") is None

    def test_set_short_code(self, mocker):
        shortcode_mock = mocker.MagicMock()
        shortcode_mock.shortcode = mocker.MagicMock()

        ShortcodeRepository().set_shortcode(shortcode_mock)

        assert shortcode_mock.shortcode in _SHORT_CODE_DB
        assert _SHORT_CODE_DB[shortcode_mock.shortcode] is shortcode_mock
